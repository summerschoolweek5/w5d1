#include "FlattenedShapeFactory.h"
#include "Elipse.h"
#include "Rectangle.h"

FlattenedShapeFactory::FlattenedShapeFactory()
{
}

IRoundShape * FlattenedShapeFactory::MakeRoundShape()
{
	return new Elipse();
}

IPolygonShape * FlattenedShapeFactory::MakePolygonShape()
{
	return new Rectangle();
}

FlattenedShapeFactory::~FlattenedShapeFactory()
{
}
