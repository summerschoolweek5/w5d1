#pragma once
#include <iostream>

class IPolygonShape
{
public:
	virtual void Print() = 0;

	virtual ~IPolygonShape() = default;
};

