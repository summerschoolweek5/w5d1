#pragma once
#include <iostream>

class IRoundShape
{
public:
	virtual void Print() = 0;

	virtual ~IRoundShape() =default;
};

