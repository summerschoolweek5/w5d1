#include <iostream>
#include "AbstractShapeFactory.h"
#include "PerfectShapeFactory.h"
#include "FlattenedShapeFactory.h"
#include "Circle.h"
#include "Elipse.h"
#include "Rectangle.h"
#include "Square.h"
#include "IRoundShape.h"
#include "IPolygonShape.h"

int main()
{

	while (1)
	{
		std::cout << "Enter a factory option:" << std::endl;
		std::cout << "1. PerfectShapeFactory" << std::endl;
		std::cout << "2. FlattenedShapeFactory" << std::endl;
		std::cout << "3. Exit" << std::endl;

		short int userInput;
		std::cin >> userInput;

		switch (userInput)
		{
		case 1:
		{
			AbstractShapeFactory* shapeFactory = new PerfectShapeFactory();

			IRoundShape* roundShape = shapeFactory->MakeRoundShape();
			IPolygonShape* polygonShape = shapeFactory->MakePolygonShape();

			std::cout << "Concrete objects from PerfectShapeFactory are: " << std::endl;
			roundShape->Print();
			polygonShape->Print();
			std::cout << std::endl;

			break;
		}

		case 2:
		{
			AbstractShapeFactory* shapeFactory = new FlattenedShapeFactory();

			IRoundShape* roundShape = shapeFactory->MakeRoundShape();
			IPolygonShape* polygonShape = shapeFactory->MakePolygonShape();

			std::cout << "Concrete objects from FlattenedShapeFactory are: " << std::endl;
			roundShape->Print();
			polygonShape->Print();
			std::cout << std::endl;

			break;
		}

		case 3:
		{
			return 0;

			break;
		}
		
		default:
			break;
		}

	}

	return 0;
}