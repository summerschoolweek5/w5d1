#include "PerfectShapeFactory.h"
#include "Circle.h"
#include "Square.h"

PerfectShapeFactory::PerfectShapeFactory()
{
}

IRoundShape* PerfectShapeFactory::MakeRoundShape()
{
	return new Circle();
}

IPolygonShape* PerfectShapeFactory::MakePolygonShape()
{
	return new Square();
}

PerfectShapeFactory::~PerfectShapeFactory()
{
}
