#pragma once
#include "AbstractShapeFactory.h"
#include "Circle.h"
#include "Square.h"

class PerfectShapeFactory : public AbstractShapeFactory
{
public:
	PerfectShapeFactory();

	IRoundShape* MakeRoundShape() override;
	IPolygonShape* MakePolygonShape() override;

	~PerfectShapeFactory();
};

