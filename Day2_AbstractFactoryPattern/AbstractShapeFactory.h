#pragma once
#include "IRoundShape.h"
#include "IPolygonShape.h"

class AbstractShapeFactory
{
public:
	virtual IRoundShape* MakeRoundShape() = 0;
	virtual IPolygonShape* MakePolygonShape() = 0;
};

