#pragma once
#include "AbstractShapeFactory.h"

class FlattenedShapeFactory : public AbstractShapeFactory
{
public:
	FlattenedShapeFactory();

	IRoundShape* MakeRoundShape() override;
	IPolygonShape* MakePolygonShape() override;

	~FlattenedShapeFactory();
};

