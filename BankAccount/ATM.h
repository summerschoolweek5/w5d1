#pragma once
#include "BankAccount.h"
#include "Account.h"
#include <iostream>

class ATM :
	public BankAccount
{
private:
	double m_lonaLimit;
	Account* m_account;
public:
	ATM(const std::string& name = "default", const double& money = 0.0, const double& lonaLimit = 1000);
	void withdraw(const double & input) override;
	double getSold() override;
	void addMoney(const double& input) override;
	~ATM();
};

