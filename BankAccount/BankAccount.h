#pragma once
class BankAccount
{
public:
	virtual void withdraw(const double & input) = 0;
	virtual double getSold() = 0;
	virtual void addMoney(const double& input) = 0;
	virtual ~BankAccount() = default;
};

