#include "ATM.h"

ATM::ATM(const std::string& name, const double& money, const double& lonaLimit): m_account(new Account(name,money)), m_lonaLimit(lonaLimit)
{
		
}

void ATM::withdraw(const double & input)
{
	if (input <= m_lonaLimit && input <= m_account->getSold())
		m_account->withdraw(input);
	else
		std::cout << "Nu\n";
}

double ATM::getSold()
{
	return m_account->getSold();
}

void ATM::addMoney(const double & input)
{
	m_account->addMoney(input);
}


ATM::~ATM()
{
}
