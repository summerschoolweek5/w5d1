#include "Account.h"



Account::Account(const std::string& name, const double& money): m_name(name), m_money(money)
{

}

void Account::withdraw(const double & input)
{
	m_money -= input;
}

double Account::getSold()
{
	return m_money;
}

void Account::addMoney(const double & input)
{
	m_money += input;
}


Account::~Account()
{
}
