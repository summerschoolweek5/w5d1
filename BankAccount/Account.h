#pragma once
#include "BankAccount.h"
#include <string>
class Account :
	public BankAccount
{
private:
	std::string m_name;
	double m_money;
public:
	Account(const std::string& name = "default",const double& money = 0.0);
	void withdraw(const double & input) override;
	double getSold() override;
	void addMoney(const double& input) override;
	~Account();
};

