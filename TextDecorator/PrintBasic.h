#pragma once
#include "Printer.h"
class PrintBasic :
	public Printer
{
public:
	PrintBasic();
	void print(const std::string& input) override;
	~PrintBasic();
};

