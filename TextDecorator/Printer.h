#pragma once
#include<string>
#include<iostream>
class Printer
{
public:
	virtual void print(const std::string& input) = 0;
	virtual ~Printer() = default;
};

