#include "Border.h"



Border::Border() : Decorator()
{

}

Border::Border(Printer* Decorator) : Decorator(Decorator)
{

}


void Border::print(const std::string & input)
{
	for (int i = 0; i < input.size(); ++i)
	{
		std::cout << '*';
	}
	std::cout << '\n';

	m_textDecorator->print(input);

	std::cout << '\n';
	for (int i = 0; i < input.size(); ++i)
	{
		std::cout << '*';
	}
}

Border::~Border()
{

}
