#pragma once
#include "Decorator.h"
class Border :
	public Decorator
{
public:
	Border();
	Border(Printer* Decorator);
	void print(const std::string& input) override;
	~Border();
};

