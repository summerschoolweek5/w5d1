#pragma once
#include "Decorator.h"
class Upper :
	public Decorator
{
public:
	Upper();
	Upper(Printer* Decorator);
	void print(const std::string& input) override;
	~Upper();
};

