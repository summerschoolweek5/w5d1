#include "Upper.h"



Upper::Upper() : Decorator()
{

}

Upper::Upper(Printer* Decorator) : Decorator(Decorator)
{

}


void Upper::print(const std::string & input)
{
	for (int i = 0; i < input.size(); ++i)
	{
		std::cout << (char)toupper(input[i]);
	}
}

Upper::~Upper()
{

}
