#pragma once
#include "Printer.h"
class Decorator :
	public Printer
{
protected:
	Printer* m_textDecorator;
public:
	Decorator();
	Decorator(Printer* Decorator);
	~Decorator();
};

